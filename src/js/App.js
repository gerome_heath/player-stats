import endpoint from "../data/player-stats.js";

const playerData = {
  ...endpoint,
  players: [...endpoint.players]
};

console.log(playerData, "playerData");
const statsEl = document.getElementById("playerStats");
const statsInfoEl = document.getElementById("playerInfo");
const selectEl = document.querySelector("#playerSelect");
const playerImageEl = document.querySelector("#playerImage");
const playerBadgeEl = document.querySelector("#playerBadge");

const insertPlayerData = () => {
  const playerArr = playerData.players.map(player => {
    const playerStats = { ...player };
    const { id, name } = playerStats.player;
    return `<option role="option" value="${id}">${name.first} ${
      name.last
    }</option>`;
  });
  const emptyOption =
    '<option role="option" value="">Select a player...</option>';
  selectEl.innerHTML = `${emptyOption}${playerArr}`;
};

insertPlayerData();

const showStats = event => {
  const { value } = event.target;

  const selectedPlayer = playerData.players.filter(player => {
    const playerStats = { ...player };
    const { id } = playerStats.player;
    const selectedID = Number(value);
    const matchedId = Number(id);
    return selectedID === matchedId;
  });

  const playerStats = selectedPlayer.map(player => {
    return player.stats;
  });

  function setSelectedOption(event) {
    const playerSelectEl = document.getElementById("playerSelect");
    const options = playerSelectEl.querySelectorAll("option");
    const optionArray = Array.from(options);
    const setSelectedOption = optionArray.filter(option => {
      const selectedPlayerId = Number(event.target.value);
      const selectedOption = Number(option.value);
      option.removeAttribute("selected");
      option.removeAttribute("aria-selected");
      if (selectedOption === selectedPlayerId) {
        option.setAttribute("selected", "selected");
        option.setAttribute("aria-selected", "true");
      }
    });
  }

  setSelectedOption(event);

  const playerInfo = selectedPlayer.map(player => {
    return player.player;
  });

  console.log("playerInfo", playerInfo);

  const selectedPlayerStats = playerStats[0] || "";
  const selectedPlayerInfo = playerInfo[0] || "";

  const { id = "", info = {}, currentTeam = {}, name = {} } =
    selectedPlayerInfo || {};
  const { first = "", last = "" } = name || {};
  const { shortName = "", name: footballTeam = "" } = currentTeam || {};
  let teamPosition;

  const position = info.position || "";

  switch (position) {
    case "D":
      teamPosition = "Defender";
      break;
    case "M":
      teamPosition = "Midfielder";
      break;
    case "F":
      teamPosition = "Forward";
      break;
    case "G":
      teamPosition = "Goal Keeper";
      break;
    default:
      teamPosition = "";
  }

  const teamName = shortName ? shortName.toLowerCase().replace(" ", "-") : "";

  function setPlayerCardBadge(teamName) {
    let badge = "";
    if (teamName) {
      badge = `<div class="player__card-badge player__card-badge--${teamName}" title="${footballTeam}"><span class="player__card-badge-text">${footballTeam}</span></div>`;
    }
    return (playerBadgeEl.innerHTML = badge);
  }

  setPlayerCardBadge(teamName);

  function setPlayerCardImage(id) {
    let image;
    if (id) {
      image = `<img src="/images/p${id}.png" class="player__card--image" title="${first} ${last}" alt="${first} ${last}"/>`;
    } else {
      image = "";
    }
    playerImageEl.innerHTML = image;
  }
  setPlayerCardImage(id);

  function setStats(stat) {
    const name = stat.name.replace("_", " ");
    const value = stat.value;
    statsInfoEl.innerHTML =
      statsInfoEl.innerHTML +
      `<div class="player__card-stats"><div class="player__card-stats-label">${name}</div><div class="player__card-stats-data">${value}</div></div>`;
  }

  statsInfoEl.innerHTML = "";

  if (selectedPlayerStats.length) {
    selectedPlayerStats.forEach(setStats);
  }

  const htmlFrag = `<div class="player__card-name">${first} ${last}</div><div class="player__card-value" >${teamPosition}</div>`;

  return (statsEl.innerHTML = htmlFrag);
};

selectEl.addEventListener("change", showStats);
